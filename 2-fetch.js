/*
Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected, and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.
*/

// Using promises and the `fetch` library, do the following.
// If you need to install `node-fetch` or a similar library, let your mentor know before doing so along with the reason why. No other external libraries are allowed to be used.
// Usage of the path library is recommended
//
// 1. Fetch all the users
// 2. Fetch all the todos
// 3. Use the promise chain and fetch the users first and then the todos.
// 4. Use the promise chain and fetch the users first and then all the details for each user.
// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

const usersEndpoint = "https://jsonplaceholder.typicode.com/users";
const todosEndpoint = "https://jsonplaceholder.typicode.com/todos";
const usersNotFound = "There was an error fetching the users data";
const todosNotFound = "There was an error fetching the todos data";
const userNotFound = "There was an error fetching the user data";
const todoNotFound = "There was an error fetching the todo data";

const userEndpoint = (id) => {
  return `https://jsonplaceholder.typicode.com/users?id=${id}`;
};

const userTodosEndpoint = (id) => {
  return `https://jsonplaceholder.typicode.com/todos?userId=${id}`;
};

const fetchData = (endpoint, errorResponse) => {
  return fetch(endpoint)
    .then((response) => {
      return response.json();
    })
    .then((response) => {
      return response;
    })
    .catch((reject) => {
      console.log(errorResponse);
      process.exit(0);
    });
};

fetchData(usersEndpoint, usersNotFound).then((response) => {
  console.log(response);
});

fetchData(todosEndpoint, todosNotFound).then((response) => {
  console.log(response);
});

let users;
let todos;
fetchData(usersEndpoint, usersNotFound)
  .then((response) => {
    users = response;
  })
  .then(() => {
    return fetchData(todosEndpoint, todosNotFound);
  })
  .then((response) => {
    todos = response;
  });

fetchData(usersEndpoint, usersNotFound)
  .then((response) => {
    users = response;
  })
  .then(() => {
    return Promise.all(
      users.map((user) => {
        return fetchData(userEndpoint(user.id), userNotFound);
      })
    );
  })
  .then((usersData) => {
    console.log(usersData.flat());
  });

let userDetails;
fetchData(todosEndpoint, todosNotFound)
  .then((response) => {
    return response[0].id;
  })
  .then((id) => {
    return fetchData(userEndpoint(id), userNotFound);
  })
  .then((response) => {
    userDetails = response[0];
  })
  .then(() => {
    return fetchData(userTodosEndpoint(userDetails.id), todosNotFound);
  })
  .then((response) => {
    userDetails.todos = response;
  })
  .then(() => {
    console.log(userDetails);
  });
